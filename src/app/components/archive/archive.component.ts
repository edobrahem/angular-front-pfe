import { AfterViewInit, Component } from '@angular/core';
import { AgenceService } from 'src/app/services/agence.service';
import { EmployeService } from 'src/app/services/employe.service';
import { StatsService } from 'src/app/services/stats.service';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.css']
})
export class ArchiveComponent implements AfterViewInit{
  showModal=false
  days=[1, 2, 3, 4, 5, 6, 7, 8, 9,10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,30, 31 ]
  today=new Date()

  currentEntry:any

  archiveDay=this.today.getDate()+1
  archiveMonth=this.today.getMonth()+1
  archiveYear=this.today.getFullYear()
  archiveAgence:string |null=null
  archiveCin:string | null=null

  archiveData:any[]=[]
  agenceNames:any[]=[]
  cinList:any=[]

  constructor(private stats:StatsService, private agence:AgenceService, private employe:EmployeService){}

  setCinList(){
    this.employe.getcinList().subscribe((data)=>{
      this.cinList=data.res
      console.log(this.cinList)
    })
  }

  getArchiveData(){
    this.archiveData=[]
    this.stats.getAllEntries(this.archiveYear, this.archiveMonth, this.archiveDay, this.archiveAgence? this.archiveAgence:null, this.archiveCin? this.archiveCin:null).subscribe((data)=>{
      this.archiveData=data.res
    })
  }

  openModif(id:number){
    this.stats.getOneEntry(id).subscribe((data)=>{
      this.currentEntry=data.res[0]
      this.toggleModal()
    })
  }

  toggleModal(){
    this.showModal=!this.showModal
  }

  updateEntry(updatedObj:any){
    console.log(updatedObj)
    this.stats.updateEntry(updatedObj).subscribe(()=>{
      this.archiveData.map((e)=>{
        if(e.id_point==updatedObj.id_point){
          e.heure=updatedObj.heure
        }
      })
      this.toggleModal()
    })
  }

  ngAfterViewInit() {
    this.setCinList()
    this.getArchiveData()
    this.agence.getNames().subscribe((data)=>{
      this.agenceNames=data.res
    })
  }

}
