import { Component } from '@angular/core';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import {faGlobe} from '@fortawesome/free-solid-svg-icons'
import { faBoxArchive } from '@fortawesome/free-solid-svg-icons';
import { SocketService } from 'src/app/services/socket.service';
import { faUsers } from '@fortawesome/free-solid-svg-icons';
import { faBuildingUser } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-app-home',
  templateUrl: './app-home.component.html',
  styleUrls: ['./app-home.component.css']
})
export class AppHomeComponent {
  isSideBarOpen:boolean=false
  faBars=faBars
  faGlobe=faGlobe
  faArchive=faBoxArchive
  faUsers=faUsers
  faBuildingUser=faBuildingUser

  /*socket
  constructor(private socketService:SocketService){
    this.socket=socketService.getSocket()
    this.socket.on('joined-room', (data)=>{
      console.log(data)
    })
    this.socket.on('notifB', (emailId)=>{
      console.log('notifB ', emailId)
    })
    this.socket.on('notification', (emailId)=>{
      console.log('notification: ',emailId)
    })
  }
  loginEmp=()=>{
    this.socket.emit('login',({roomId:'Employee', emailId:'chdo@gmail.com'}))

  }
  loginAdmin=()=>{
    this.socket.emit('login',({roomId:'administrator', emailId:'edobrahem@gmail.com'}))

  }
  callEmpSocket=()=>{
    this.socketService.sendNotif({roomId:'Employee', emailId:'chdo@gmail.com'})
    
  }

  callAdminSocket=()=>{
    this.socketService.sendNotif({roomId:'administrator', emailId:'edobrahem@gmail.com'})
    
  }
  callHim=()=>{
    console.log("psssst")
    this.socket.emit('notif',({emailId:'edobrahem@gmail.com'}));
    
  }
  disconnect=()=>{
    this.socket.emit('logout');
  }*/

  expand(){
    this.isSideBarOpen=!this.isSideBarOpen
  }

  closeAfterNav(){
    if(this.isSideBarOpen){
      this.isSideBarOpen=!this.isSideBarOpen
    }
    
  }

  sideStyle(){
    if(this.isSideBarOpen){
      return 'width: 20%;'
    }else{
      return 'width: 5%;'
    }
    
  }

  mainStyle(){
    if(this.isSideBarOpen){
      return 'opacity: 20%; '
    }else{
      return ''
    }
  }

  navTextStyle(){
    if(this.isSideBarOpen){
      return 'margin-right: 5rem; color: #b6c6ed;'
    }else{
      return 'opacity:0%;'
    }
  }

}
