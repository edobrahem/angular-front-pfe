import { Component, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { D3Service } from 'src/app/services/d3.service';
import { StatsService } from 'src/app/services/stats.service';
import {faAnglesUp} from '@fortawesome/free-solid-svg-icons'
import {faAnglesDown} from '@fortawesome/free-solid-svg-icons'
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements AfterViewInit{
  dayPresCompare=true
  faAnglesUp=faAnglesUp
  faAnglesDown=faAnglesDown

   months=["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"] 
   days=[1, 2, 3, 4, 5, 6, 7, 8, 9,10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,30, 31 ]
   dayValues:any[]=[]
   monthValues:any[]=[]
   today=new Date()

   trendYear:number=this.today.getFullYear()
   trendMonth:number=0

   dayPieDay:number=this.today.getDate()
   dayPieMonth:number=this.today.getMonth()+1
   dayPieYear:number=this.today.getFullYear()
   dayPieRegion:string | null=null

   monthPieMonth:number=this.today.getMonth()+1
   monthPieYear:number=this.today.getFullYear()
   monthPieRegion:string|null=null

   currentmonth=this.months[new Date().getMonth()]

  
  constructor(private stats:StatsService,private D3:D3Service){
  }

  @ViewChild('curveContainer') curveContainer!:ElementRef

  @ViewChild('dayPiePres')  dayPiePresContainer!:ElementRef
  @ViewChild('dayPiePonct') dayPiePonctContainer!:ElementRef
  @ViewChild('dayPieLeav')  dayPieLeavContainer!:ElementRef

  @ViewChild('monthPiePres')  monthPiePresContainer!:ElementRef
  @ViewChild('monthPiePonct') monthPiePonctContainer!:ElementRef
  @ViewChild('monthPieLeav')  monthPieLeavContainer!:ElementRef

  getTrendData(){
    let dataPresence:[string, number][] = [];
    let dataPonct:[string, number][] = [];
    let dataRetard:[string, number][] = [];  

    this.stats.getCurveData(this.trendYear,this.trendMonth).subscribe((data)=>{
      data.res.forEach((element)=>{
        dataPresence.push([element[0], element[1]])
        dataPonct.push([element[0], element[3]])
        dataRetard.push([element[0], element[4]])
      })
      this.D3.removeD3Element(this.curveContainer)
      this.D3.drawTrendCurve(this.curveContainer, dataPresence, dataPonct, dataRetard, this.trendYear)
    })
  }

  getDayPieData(){
    
    this.dayValues=[]
    this.D3.removeD3Element(this.dayPiePresContainer)
    this.D3.removeD3Element(this.dayPiePonctContainer)
    this.D3.removeD3Element(this.dayPieLeavContainer)
    this.stats.getPieData(this.dayPieYear, this.dayPieMonth, this.dayPieDay, this.dayPieRegion? this.dayPieRegion:null).subscribe((data)=>{
      this.dayValues.push(data.res[0], data.res[1], data.res[2], data.res[3]? data.res[3]:null, data.res[4]? data.res[4]:null, data.res[5]? data.res[5]:null )
      const PresData=[data.res[0], {label:'Absence', value:100-data.res[0].value}]
      const PonctData=[data.res[1], {label:'Arrivée Retard', value:100-data.res[1].value}]
      const LeavData=[data.res[2], {label:"Sortie a l'heure", value:100-data.res[2].value}]
      this.D3.drawPie(this.dayPiePresContainer, PresData, ['steelblue', '#d9534f'])
      this.D3.drawPie(this.dayPiePonctContainer, PonctData, ['#007E33', '#ffbb33'])
      this.D3.drawPie(this.dayPieLeavContainer, LeavData, ['#8c0c1c', "#9cbfdd"])
      console.log(this.dayValues)
    })
  }

  getMonthPieData(){
    this.monthValues=[]
    this.D3.removeD3Element(this.monthPiePresContainer)
    this.D3.removeD3Element(this.monthPiePonctContainer)
    this.D3.removeD3Element(this.monthPieLeavContainer)
    
    this.stats.getPieData(this.monthPieYear, this.monthPieMonth, null, this.monthPieRegion? this.monthPieRegion:null).subscribe((data)=>{
      this.monthValues.push(data.res[0], data.res[1], data.res[2])
      this.stats.getPieData(this.monthPieYear, this.monthPieMonth-1).subscribe((dataprev)=>{
        this.monthValues.push(dataprev.res[0], dataprev.res[1], dataprev.res[2])
        const PresData=[data.res[0], {label:'Absence', value:100-data.res[0].value}]
      const PonctData=[data.res[1], {label:'Arrivée Retard', value:100-data.res[1].value}]
      const LeavData=[data.res[2], {label:"Sortie a l'heure", value:100-data.res[2].value}]
      this.D3.drawPie(this.monthPiePresContainer, PresData, ['steelblue', '#d9534f'])
      this.D3.drawPie(this.monthPiePonctContainer, PonctData, ['#007E33', '#ffbb33'])
      this.D3.drawPie(this.monthPieLeavContainer, LeavData, ['#9cbfdd', "#8c0c1c"])
      console.log(this.monthValues)
      })
    })
  }

  ngAfterViewInit(){
    this.getTrendData()
    this.getDayPieData()
    this.getMonthPieData()
  }

}
