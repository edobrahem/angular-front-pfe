import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AgenceService } from 'src/app/services/agence.service';
import { EmployeService } from 'src/app/services/employe.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit{
  currentEmployee:any
  agenceNames:any[]=[]
  currentAgence:any
  currentTel:any
  currentUrgence:any
  currentDepartment:any
  currentposition:any
  disableDelete=false
  i=0

  constructor(
    private activatedRoute:ActivatedRoute, 
    private employe:EmployeService, 
    private agence:AgenceService,
    private router:Router
    ){}
  sendUpdate(){
    this.employe.updateOne({
      numTel:this.currentTel, 
      idLocale:this.currentAgence, 
      contactUrgence:this.currentUrgence, 
      cin:this.currentEmployee.cinEmploye, 
      departement:this.currentDepartment, 
      position:this.currentposition
    }).subscribe(()=>{
      console.log('updated')
    })
  }

  sendDelete(){
    if(this.i===0){
      this.disableDelete=!this.disableDelete
      setTimeout(()=>this.disableDelete=!this.disableDelete, 5000)
      this.i++
    }else{
      this.employe.deleteOne(this.currentEmployee._id).subscribe(()=>{this.router.navigate(['/home/employes'])})
    }
    
  }

  ngOnInit(): void {
    this.agence.getNames().subscribe((data)=>{
      this.agenceNames=data.res
    })
    this.activatedRoute.params.subscribe(paramsRes=>{
      this.employe.getOne(paramsRes['id']).subscribe(emp=>{
        this.currentEmployee=emp
        this.currentAgence=emp.idLocale
        this.currentTel=emp.numTel
        this.currentDepartment=emp.departement
        this.currentposition=emp.position
        this.currentUrgence=emp.contactUrgence
        console.log(this.currentEmployee)
      })
    })
  }
}
