import { AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import { StatsService } from 'src/app/services/stats.service';
@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements AfterViewInit{
  @ViewChild('qrContainer') qrContainer!:ElementRef
  tst:string='exampple'

  qrCodeData: string='empty';

  constructor(private statsService:StatsService){
    setInterval(()=>{
      this.currentTime=new Date().toLocaleTimeString()
    }, 1000)
  }

  dataReceived:boolean=false
  currentTime!:string

    

   generateQR(){
    try{
      this.statsService.getQRToken().subscribe(async (data)=>{
        this.dataReceived=true
        this.qrCodeData =data.clockInToken;
      })
      
      
    }catch(e){
      this.dataReceived=false
      console.log(e)
    }
  }

ngAfterViewInit(): void {

   this.generateQR()
   
}
    

}
