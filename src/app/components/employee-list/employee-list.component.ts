import { Component, OnInit } from '@angular/core';
import { EmployeService } from 'src/app/services/employe.service';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { AgenceService } from 'src/app/services/agence.service';
@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit{
  ListeEmployee:any[]=[]
  filteredList:any[]=[]
  cinList:String[]=[]
  agenceNames:any[]=[]

  agenceFilter=null
  cinFilter=null
  faChevronRight=faChevronRight

  constructor(private employe:EmployeService, private agence:AgenceService){

  }
  setCinList(){
    this.employe.getcinList().subscribe((data)=>{
      this.cinList=data.res
      console.log(this.cinList)
    })
  }

  getAllEmps(){
    this.employe.getAll().subscribe(data=>{
      this.ListeEmployee=data.res
      this.filterresult()
      console.log(this.ListeEmployee)
    })
  }

  filterresult(){
    if(this.cinFilter!=null && this.agenceFilter!=null){
      this.filteredList=this.ListeEmployee.filter((emp)=>
        emp.libelAgence==this.agenceFilter && emp.cin==this.cinFilter
      )
    }else if(this.cinFilter!=null && this.agenceFilter==null){
      this.filteredList=this.ListeEmployee.filter((emp)=>
         emp.cin==this.cinFilter
      )
    }
    else if(this.cinFilter==null && this.agenceFilter!=null){
      this.filteredList=this.ListeEmployee.filter((emp)=>
        emp.libelAgence==this.agenceFilter
      )
    }else{
      this.filteredList=this.ListeEmployee
    }
  }

  

  ngOnInit(): void {
    this.getAllEmps()
    this.setCinList()
    this.agence.getNames().subscribe((data)=>{
      this.agenceNames=data.res
    })

  }

}
