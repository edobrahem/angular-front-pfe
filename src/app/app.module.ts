import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './components/LandingPage/landing-page/landing-page.component';
import { AppHomeComponent } from './components/AppHome/app-home/app-home.component';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { ArchiveComponent } from './components/archive/archive.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { EmployeeDetailComponent } from './components/employee-detail/employee-detail.component';
import { AgenceListComponent } from './components/agence-list/agence-list.component';


@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    AppHomeComponent,
    DashboardComponent,
    ArchiveComponent,
    EmployeeListComponent,
    EmployeeDetailComponent,
    AgenceListComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FontAwesomeModule,
    FormsModule,
    NgxQRCodeModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
