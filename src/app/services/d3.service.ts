import { ElementRef, Injectable } from '@angular/core';
import * as d3 from 'd3'
@Injectable({
  providedIn: 'root'
})
export class D3Service {

  constructor() { }

  removeD3Element(curveContainer:ElementRef){
    d3.select(curveContainer.nativeElement).selectAll('*').remove()
  }

  drawPie(pieContainer:ElementRef, data:any[], colors:string[]){
    
    const svg = d3.select(pieContainer.nativeElement);
    const width = +svg.attr('width');
    const height = +svg.attr('height');
    const radius = Math.min(width, height) / 2;
  
    const pie=d3.pie<{label:string;value:number}>()
    .value(d=>d.value)

    const arc=d3.arc<d3.PieArcDatum<{label:string, value:number}>>()
    .innerRadius(50)
    .outerRadius(radius)

    const color=d3.scaleOrdinal<string>()
    .range(colors)

    

    const arcs=svg.selectAll('arc')
    .data(pie(data))
    .enter()
    .append('g')
    .attr('class', 'arc')
    .attr('transform',`translate(${width/2}, ${height / 2})` )

    arcs.append('path')
    .attr('d', arc)
    .attr('fill', d => color(d.data.label))
    .transition() 
    .duration(800) 
    .attrTween('d', (d) =>{
      const interpolate = d3.interpolate({ startAngle: 0, endAngle: 0 }, d);
      return  (t) =>{
        const interpolatedArc=interpolate(t)
        return interpolatedArc ? arc(interpolatedArc)! : ""
      };
    })
  }

  drawTrendCurve(curveContainer:ElementRef, dataPresence:[string, number][], dataPonct:[string, number][], dataRetr:[string, number][], year:number){
    const margin={top:20, right:20, bottom:30, left:30}
    const width=1200-margin.left-margin.right
    const height=500-margin.top-margin.bottom

    const svg = d3.select(curveContainer.nativeElement)
    .attr('width', width+margin.left+margin.right)
    .attr('height', height+margin.top+margin.bottom)
    .attr('background-color', '')
    .append('g')
    .attr('transform', `translate(${margin.left}, ${margin.top})`)

    svg.selectAll('*').remove()

    const xScale=d3.scaleBand()
    .domain(dataPresence.map(d=>d[0]))
    .range([0,width])
    .padding(0.1)

    const yScale=d3.scaleLinear()
    .domain([0, 100])
    .range([height, 0])

    const x_axis=d3.axisTop(xScale)
    const y_axis=d3.axisLeft(yScale)

    const gridGroup=svg.append('g')
    
    const yGrid=d3.axisLeft(yScale)
    .tickSize(-width)
    .tickFormat(()=>"")
    .tickValues(yScale.ticks().filter((d, i)=>d%2==0))
    
    gridGroup.append('g')
    .attr('color', 'lightgray')
    .call(yGrid as any, undefined)

    svg.append('g')
    .style('font-size', '12px')
    .style('font-weight', 'bold')
    .attr('transform', `translate(0, ${height})`)
    .call(x_axis)
    .attr('color', 'gray')
    .selectAll('text')
    .attr('y', 9)
    .attr('dy', '0.35em')
    .style('text-anchor', 'middle')

    svg.append('g')
    .call(y_axis)
    .attr('color', 'gray')
    .style('font-size', '12px')
    .style('font-weight', 'bold')

    /*-------------------Courbe Presence---------------------- */

    const linePresence = d3.line<[string, number]>()
      .x((d, i) => xScale(d[0])!+xScale.bandwidth()/2 )
      .y(d => yScale(d[1]))
      .curve(d3.curveMonotoneX)

    const pathPresence=svg.append('path')
    .datum(dataPresence)
    .attr('d', linePresence)
    .attr('stroke', 'steelblue')
    .attr('stroke-width', 3)
    .attr('fill', 'none')

    const totalLengthPresence=pathPresence.node()!.getTotalLength()

    pathPresence.attr('stroke-dasharray', totalLengthPresence+' '+totalLengthPresence)
    .attr('stroke-dashoffset', totalLengthPresence)
    .transition()
    .duration(1500)
    .ease(d3.easeLinear)
    .attr('stroke-dashoffset', 0)

    const circleGroupPresence=svg.append('g')
    const tooltip=d3.select('#tooltip')
    

    circleGroupPresence.selectAll('circle')
    .data(dataPresence)
    .enter()
    .append('circle')
    .attr('cx', (d)=>xScale(d[0])!+xScale.bandwidth()/2)
    .attr('cy', (d)=>yScale(d[1]))
    .attr('r', 0)
    .attr('fill', 'steelblue')
    .on('mouseover', (event, d)=>{
      d3.select(event.currentTarget)
      .transition()
      .duration(200)
      .attr('r', 6)
      tooltip
        .style('display', 'flex')
        .style('border', ' 2px solid gray ')
        .style('padding', '5px')
        .style('background-color', 'rgb(177, 208, 233)')
        .attr('class', 'rounded')
        .style('left', (event.pageX-50) + 'px')
        .style('top', (event.pageY-40) + 'px')
        .text(`${d[0]}-${year}, ${ Math.round(d[1])}% Taux de Présence`)
    })
    .on('mouseout', (event) => {
      tooltip.style('display', 'none')
      d3.select(event.currentTarget)
      .transition()
      .duration(200)
      .attr('r', 4)
    })
    .transition()
    .delay((d, i)=>i*100)
    .duration(500)
    .attr('r', 4)

    /*--------------------------Courbe Ponctualite--------------------------- */

    const linePonct = d3.line<[string, number]>()
      .x((d, i) => xScale(d[0])!+xScale.bandwidth()/2 )
      .y(d => yScale(d[1]))
      .curve(d3.curveMonotoneX)

    const pathPonct=svg.append('path')
    .datum(dataPonct)
    .attr('d', linePonct)
    .attr('stroke', '#007E33')
    .attr('stroke-width', 3)
    .attr('fill', 'none')

    const totalLengthPonct=pathPonct.node()!.getTotalLength()

    pathPonct.attr('stroke-dasharray', totalLengthPonct+' '+totalLengthPonct)
    .attr('stroke-dashoffset', totalLengthPonct)
    .transition()
    .duration(1500)
    .ease(d3.easeLinear)
    .attr('stroke-dashoffset', 0)

    const circleGroupPonct=svg.append('g')
    

    circleGroupPonct.selectAll('circle')
    .data(dataPonct)
    .enter()
    .append('circle')
    .attr('cx', (d)=>xScale(d[0])!+xScale.bandwidth()/2)
    .attr('cy', (d)=>yScale(d[1]))
    .attr('r', 0)
    .attr('fill', '#007E33')
    .on('mouseover', (event, d)=>{
      d3.select(event.currentTarget)
      .transition()
      .duration(200)
      .attr('r', 6)
      tooltip
        .style('display', 'flex')
        .style('padding', '5px')
        .style('border', ' 2px solid gray ')
        .style('background-color', '#24b45e')
        .attr('class', 'rounded')
        .style('left', (event.pageX-50) + 'px')
        .style('top', (event.pageY-40) + 'px')
        .text(`${d[0]}-${year}, ${ Math.round(d[1])}% Taux de Ponctualité`)
    })
    .on('mouseout', (event) => {
      tooltip.style('display', 'none')
      d3.select(event.currentTarget)
      .transition()
      .duration(200)
      .attr('r', 4)
    })
    .transition()
    .delay((d, i)=>i*100)
    .duration(500)
    .attr('r', 4)
    
  /*-----------------------courbe retard----------------------------------------- */

  const lineRetard = d3.line<[string, number]>()
      .x((d, i) => xScale(d[0])!+xScale.bandwidth()/2 )
      .y(d => yScale(d[1]))
      .curve(d3.curveMonotoneX)

    const pathRetard=svg.append('path')
    .datum(dataRetr)
    .attr('d', lineRetard)
    .attr('stroke', '#ffbb33')
    .attr('stroke-width', 3)
    .attr('fill', 'none')

    const totalLengthRetard=pathRetard.node()!.getTotalLength()

    pathRetard.attr('stroke-dasharray', totalLengthRetard+' '+totalLengthRetard)
    .attr('stroke-dashoffset', totalLengthRetard)
    .transition()
    .duration(1500)
    .ease(d3.easeLinear)
    .attr('stroke-dashoffset', 0)

    const circleGroupRetard=svg.append('g')
    

    circleGroupRetard.selectAll('circle')
    .data(dataRetr)
    .enter()
    .append('circle')
    .attr('cx', (d)=>xScale(d[0])!+xScale.bandwidth()/2)
    .attr('cy', (d)=>yScale(d[1]))
    .attr('r', 0)
    .attr('fill', '#ffbb33')
    .on('mouseover', (event, d)=>{
      d3.select(event.currentTarget)
      .transition()
      .duration(200)
      .attr('r', 6)
      tooltip
        .style('display', 'flex')
        .style('padding', '5px')
        .style('border', ' 2px solid gray ')
        .style('background-color', '#f7cf80')
        .attr('class', 'rounded')
        .style('left', (event.pageX-50) + 'px')
        .style('top', (event.pageY-40) + 'px')
        .text(`${d[0]}-${year}, ${ Math.round(d[1])}% Taux de Sortie avant l'heure`)
    })
    .on('mouseout', (event) => {
      tooltip.style('display', 'none')
      d3.select(event.currentTarget)
      .transition()
      .duration(200)
      .attr('r', 4)
    })
    .transition()
    .delay((d, i)=>i*100)
    .duration(500)
    .attr('r', 4)
  }
  
}
