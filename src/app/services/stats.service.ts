import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
interface CurveDataResponse {
  res:[string, number, number, number, number][]
  year:number
  month:number
}
interface PieDataResponse {
  res:{label:string, value:number}[]
  year:number
  month:number
  day:number
}

interface tokenResponse{
  clockInToken:string
}

interface EntriesResponse{
  res:{id_point:number, cin_employe:string, id_local:string, seance:string, date:string, heure:string, type:string}[]
}

interface OneEntryResponse{
  res:{id_point:number, cin_employe:string, seance:string, date:string, heure:string, type:string}[]
}
@Injectable({
  providedIn: 'root'
})
export class StatsService {

  constructor(private http:HttpClient){ }

  getCurveData(year:number, month:number):Observable<CurveDataResponse> {
    return this.http.get<CurveDataResponse>(`http://localhost:3002/getTrend?year=${year}&month=${month}`)
  }

  getPieData(year:number, month:number, day?:number | null, region?:string| null):Observable<PieDataResponse> {
    console.log('the selected month : '+month+' the select day : '+day+'  the selected region : '+region)
    if(day===null || day===undefined){
      if(region===null||region==undefined){
        return this.http.get<PieDataResponse>(`http://localhost:3002/getMonthPie?year=${year}&month=${month}&day=${day}`)
      }else{
        return this.http.get<PieDataResponse>(`http://localhost:3002/getMonthPie?year=${year}&month=${month}&day=${day}&region=${region}`)
      }
      
    }else{
      if(region===null || region === undefined){
        return this.http.get<PieDataResponse>(`http://localhost:3002/getDayPie?year=${year}&month=${month}&day=${day}`)
      }else{
        return this.http.get<PieDataResponse>(`http://localhost:3002/getDayPie?year=${year}&month=${month}&day=${day}&region=${region}`)
      }
    }
    
  }

  getQRToken():Observable<tokenResponse>{
    return this.http.get<tokenResponse>('http://localhost:3002/clockToken')
  }

  getAllEntries(year:number, month:number, day:number, agence?:string | null, cin?:string|null):Observable<EntriesResponse>{
    if(agence){
      if(cin){
        return this.http.get<EntriesResponse>(`http://localhost:3002/entries?year=${year}&month=${month}&day=${day}&agence=${agence}&cin=${cin}`)
      }else{
        return this.http.get<EntriesResponse>(`http://localhost:3002/entries?year=${year}&month=${month}&day=${day}&agence=${agence}`)
      }
    }else{
      if(cin){
        return this.http.get<EntriesResponse>(`http://localhost:3002/entries?year=${year}&month=${month}&day=${day}&cin=${cin}`)
      }else{
        return this.http.get<EntriesResponse>(`http://localhost:3002/entries?year=${year}&month=${month}&day=${day}`)
      }
    }
  }

  getOneEntry(id:number):Observable<OneEntryResponse>{
    return this.http.get<OneEntryResponse>(`http://localhost:3002/oneEntry?id=${id}`)
  }

  updateEntry(updatedObj:any){
    const body={heure:updatedObj.heure, id:updatedObj.id_point}
  
    return this.http.put("http://localhost:3002/corrig", body)
  }
  
}
