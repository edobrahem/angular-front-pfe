import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


interface CinResponse{
  res:string[]
}


interface EmpListResponse{
  res:{
  id:String,
  cin:String,
  nomComplet:String,
  libelAgence:String,
  numTel:String,
  position:String}[]
}
@Injectable({
  providedIn: 'root'
})
export class EmployeService {

  constructor(private http:HttpClient) { }

  getcinList():Observable<CinResponse>{
    return this.http.get<CinResponse>('http://localhost:3001/infoService/employes/getCin')
  }

  getAll():Observable<EmpListResponse>{
      return this.http.get<EmpListResponse>("http://localhost:3001/infoService/employes/")
  }

  getOne(id:string):Observable<any>{
    return this.http.get<any>("http://localhost:3001/infoService/employes/show?id="+id)
  }

  updateOne(updateObj:any){
    const body={
      numTel:updateObj.numTel,
      idLocale:updateObj.idLocale,
      contactUrgence:updateObj.contactUrgence,
      position:updateObj.position,
      departement:updateObj.departement,
      cin:updateObj.cin
    }
    return this.http.put("http://localhost:3001/infoService/employes/update", body)
  }

  deleteOne(id:string){
    const body={id:id}
    return this.http.delete("http://localhost:3001/infoService/employes/delete", {body:body})
  }
}
