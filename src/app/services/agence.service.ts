import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

interface NamesResponse{
  res:{_id:string, nomAgence:string}[]
}

@Injectable({
  providedIn: 'root'
})
export class AgenceService {

  constructor(private http:HttpClient) { }

  getNames():Observable<NamesResponse>{
    return this.http.get<NamesResponse>('http://localhost:3001/infoService/agences/getNames')
  }

}
