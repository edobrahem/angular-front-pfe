import { Injectable } from '@angular/core';
import { io } from 'socket.io-client';
@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private socket

  getSocket(){
    return this.socket
  }

  constructor() {
    this.socket=io('http://localhost:3000')
  
  }

  sendNotif=(data:any)=>{
    this.socket.emit("broadcastNotif", data)
  }
}
