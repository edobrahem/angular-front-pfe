import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './components/LandingPage/landing-page/landing-page.component';
import { AppHomeComponent } from './components/AppHome/app-home/app-home.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ArchiveComponent } from './components/archive/archive.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { AgenceListComponent } from './components/agence-list/agence-list.component';
import { EmployeeDetailComponent } from './components/employee-detail/employee-detail.component';

const routes: Routes = [
  {path:'home', component:AppHomeComponent, children:[
    {path:'dashboard', component:DashboardComponent}, 
    {path:'archive', component:ArchiveComponent},
    {path:'employes', component:EmployeeListComponent},
    {path:'employes/:id', component:EmployeeDetailComponent},  
    {path:'agences', component:AgenceListComponent},  
    {path:'', redirectTo:"dashboard", pathMatch:"full"}
    
  ]},
  {path:'landing', component:LandingPageComponent},
  {path:'', redirectTo:'landing', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
